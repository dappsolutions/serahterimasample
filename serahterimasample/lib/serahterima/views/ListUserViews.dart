import 'package:flutter/material.dart';

import 'SerahTerimaView.dart';

class ListUserView extends StatefulWidget {
  @override
  _ListUserViewState createState() => _ListUserViewState();
}

class _ListUserViewState extends State<ListUserView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("User Serah Terima"),
        backgroundColor: Colors.green,
      ),
      body:Container(
        child: Column(
          children: <Widget>[
//            SizedBox(
//              height: 20,
//            ),
//            Center(
//              child: RaisedButton(
//                onPressed: (){
//                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
//                    userFinger: "liyan",
//                    serahTerima: "serah",
//                  )));
//                },
//                child: Text("LIYAN"),
//                color: Colors.orange,
//              )
//            ),
//            SizedBox(
//              height: 20,
//            ),
//            Center(
//              child: RaisedButton(
//                onPressed: (){
//                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
//                    userFinger: "deni",
//                    serahTerima: "serah",
//                  )));
//                },
//                child: Text("DENI"),
//                color: Colors.orange,
//              )
//            ),
//            SizedBox(
//              height: 20,
//            ),
//            Center(
//              child: RaisedButton(
//                onPressed: (){
//                  Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
//                    userFinger: "purwanto",
//                    serahTerima: "serah",
//                  )));
//                },
//                child: Text("PURWANTO"),
//                color: Colors.orange,
//              )
//            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Container(
                width: 200,
                height: 60,
                child: RaisedButton(
                  onPressed: (){
                    Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                      userFinger: "dimas",
                      serahTerima: "terima",
                    )));
                  },
                  child: Text("DIMAS"),
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(18.0),
                      side: BorderSide(color: Colors.green)
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
                child: Container(
                  width: 200,
                  height: 60,
                  child: RaisedButton(
                    onPressed: (){
                      Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                        userFinger: "rozi",
                        serahTerima: "terima",
                      )));
                    },
                    child: Text("ROZI"),
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.green)
                    ),
                  ),
                )
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      )
    );
  }
}
